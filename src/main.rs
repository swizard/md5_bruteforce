#![feature(collections, std_misc)]
extern crate crypto;
extern crate sys_info;
extern crate rustc_serialize;

use std::{io, fmt};
use std::iter::Iterator;
use std::io::{Write, BufRead};
use std::sync::mpsc;
use std::thread::{JoinHandle, spawn};
use crypto::md5::Md5;
use crypto::digest::Digest;
use rustc_serialize::hex::{ToHex, FromHex, FromHexError};

#[derive(Debug)]
enum Md5Error {
    Parse(FromHexError),
    InvalidLength,
}

#[derive(Debug)]
enum RangeError {
    Empty,
    DifferentLengths,
    LowerIsGreater,
    IllegalSymbol(char),
}

#[derive(Debug)]
enum ParseError {
    EmptyString,
    NoLowerRange,
    NoUpperRange,
    TrailingGarbage(String),
}

#[derive(Debug)]
enum TaskError {
    Parse(ParseError),
    Md5(Md5Error, String),
    Range(RangeError, (String, String)),
}

#[derive(Debug)]
enum TaskResult {
    NotFound(SlaveTask),
    Found(SlaveTask, String),
    Error(String, TaskError),
    Panic,
}

#[derive(Debug)]
struct SlaveTask {
    md5: Vec<u8>,
    lower: Vec<u8>,
    upper: Vec<u8>,
}

impl fmt::Display for SlaveTask {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{{}, [{},{}]}}", 
               self.md5.to_hex(),
               String::from_utf8_lossy(&self.lower[..]),
               String::from_utf8_lossy(&self.upper[..]))
    }
}

impl SlaveTask {
    fn parse(line: &str) -> Result<SlaveTask, TaskError> {
        if line.is_empty() {
            return Err(TaskError::Parse(ParseError::EmptyString));
        }

        let mut chunks = line
            .trim_right_matches(|c: char| c.is_whitespace())
            .split(|c: char| c.is_whitespace());
        match (chunks.next(), chunks.next(), chunks.next(), chunks.next()) {
            (None, _, _, _) => 
                Err(TaskError::Parse(ParseError::EmptyString)),
            (Some(_), None, _, _) => 
                Err(TaskError::Parse(ParseError::NoLowerRange)),
            (Some(_), Some(_), None, _) => 
                Err(TaskError::Parse(ParseError::NoUpperRange)),
            (Some(_), Some(_), Some(_), Some(garbage)) => 
                Err(TaskError::Parse(ParseError::TrailingGarbage(garbage.to_string()))),
            (Some(md5), Some(lower), Some(upper), None) => {
                let md5_buf = try!(md5.from_hex().map_err(|e| TaskError::Md5(Md5Error::Parse(e), md5.to_string())));
                let range_lower = try!(parse_range(lower).map_err(|e| TaskError::Range(e, (lower.to_string(), upper.to_string()))));
                let range_upper = try!(parse_range(upper).map_err(|e| TaskError::Range(e, (lower.to_string(), upper.to_string()))));
                match (md5_buf.len() == 16, range_lower.len() == range_upper.len(), range_lower <= range_upper) {
                    (true, true, true) =>
                        Ok(SlaveTask { md5: md5_buf, lower: range_lower, upper: range_upper, }),
                    (false, _, _) => 
                        Err(TaskError::Md5(Md5Error::InvalidLength, md5.to_string())),
                    (_, false, _) =>
                        Err(TaskError::Range(RangeError::DifferentLengths, (lower.to_string(), upper.to_string()))),
                    (_, _, false) =>
                        Err(TaskError::Range(RangeError::LowerIsGreater, (lower.to_string(), upper.to_string()))),
                }
            },
        }
    }
}

fn parse_range(range: &str) -> Result<Vec<u8>, RangeError> {
    if range.is_empty() {
        return Err(RangeError::Empty)
    } 

    range.chars()
        .map(|ch| if (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'z') {
            Ok(ch as u32 as u8)
        } else {
            Err(RangeError::IllegalSymbol(ch))
        })
        .collect()
}

#[derive(Debug)]
enum Command {
    Workload(String),
    Terminate,
}

fn next_byte(x: u8) -> u8 {
    match x as char {
        '9' => 'a' as u32 as u8,
        'z' => '0' as u32 as u8,
        _ => x + 1,
    }
}
 
fn next_pass(buf: &mut [u8]) {
    for b in buf.iter_mut().rev() {
        *b = next_byte(*b);
        if *b as char != '0' {
            break
        }
    }
}

fn bruteforce(md5: &mut Md5, 
              md5_buf: &mut [u8], 
              guess_buf: &mut Vec<u8>, 
              &SlaveTask { md5: ref sample, lower: ref range_from, upper: ref range_to }: &SlaveTask) -> Option<String> 
{
    guess_buf.clear();
    guess_buf.push_all(&range_from[..]);
    loop {
        md5.reset();
        md5.input(&guess_buf);
        md5.result(md5_buf);
        if sample == &md5_buf {
            return Some(guess_buf.iter().map(|&byte| byte as char).collect())
        } else if guess_buf[..] == range_to[..] {
            return None
        }
        next_pass(&mut guess_buf[..]);
    }
}

fn slave_loop(rx: mpsc::Receiver<Command>, tx: mpsc::Sender<TaskResult>) {
    let mut md5 = Md5::new();
    let mut md5_buf: [u8; 16] = [0; 16];
    let mut guess_buf = Vec::new();

    loop {
        match rx.recv() {
            Ok(Command::Workload(line)) => {
                match SlaveTask::parse(&line) {
                    Ok(task) => {
                        tx.send(if let Some(found) = bruteforce(&mut md5, &mut md5_buf, &mut guess_buf, &task) {
                            TaskResult::Found(task, found)
                        } else {
                            TaskResult::NotFound(task)
                        }).unwrap()
                    },
                    Err(error) => {
                        tx.send(TaskResult::Error(line, error)).unwrap();
                        match rx.recv() {
                            Ok(Command::Terminate) => break,
                            other => panic!("termination ack waiting, but {:?} received", other),
                        }
                    },
                }
            },
            Ok(Command::Terminate) | Err(..) =>
                break,
        }
    }
}

struct SlavePeer {
    thread: JoinHandle,
    tx: mpsc::Sender<Command>,
    rx: mpsc::Receiver<TaskResult>,
}

struct SlavesPool {
    slaves: Vec<Option<SlavePeer>>,
}

impl SlavesPool {
    fn new(slaves_limit: usize) -> SlavesPool {
        let mut slaves = Vec::with_capacity(slaves_limit);
        for _ in 0 .. slaves_limit {
            slaves.push(None)
        }
        SlavesPool { slaves: slaves, }
    }

    fn slave(&self, slave_idx: usize) -> &SlavePeer { self.slaves[slave_idx].as_ref().unwrap() }
    fn slave_mut(&mut self, slave_idx: usize) -> &mut SlavePeer { self.slaves[slave_idx].as_mut().unwrap() }

    fn join_terminate(&mut self, slave_idx: usize) {
        self.slave_mut(slave_idx).tx.send(Command::Terminate).unwrap();
        self.join(slave_idx);
    }

    fn join(&mut self, slave_idx: usize) {
        if let Some(slave) = self.slaves[slave_idx].take() {
            let _ = slave.thread.join();
        }
    }
}

struct Manager {
    pool: SlavesPool,
    idle: Vec<usize>,
    busy: Vec<usize>,
    free: Vec<usize>,
}

impl Drop for Manager {
    fn drop(&mut self) {
        while !self.all_finished() {
            let _ = self.poll();
        }

        for &idle_idx in self.idle.iter() {
            self.pool.join_terminate(idle_idx);
        }
    }
}

impl Manager {
    fn new() -> Manager {
        let cpus_count = sys_info::cpu_num().unwrap() as usize;
        Manager { 
            pool: SlavesPool::new(cpus_count),
            idle: Vec::with_capacity(cpus_count),
            busy: Vec::with_capacity(cpus_count),
            free: (0 .. cpus_count).collect(),
        }
    }
    
    fn available(&self) -> bool { !self.idle.is_empty() || !self.free.is_empty() }
    fn all_finished(&self) -> bool { self.busy.is_empty() }

    fn feed_workload(&mut self, line: String) {
        let ready_slave_idx = if let Some(idle_index) = self.idle.pop() {
            // use existing idle slave from pool
            idle_index
        } else if let Some(free_index) = self.free.pop() {
            // spawn another slave
            let (req_tx, req_rx) = mpsc::channel();
            let (rep_tx, rep_rx) = mpsc::channel();
            let handle = spawn(move || { slave_loop(req_rx, rep_tx); });
            let slave = Some(SlavePeer { thread: handle, tx: req_tx, rx: rep_rx, });
            std::mem::replace(&mut self.pool.slaves[free_index], slave);
            free_index
        } else {
            unreachable!()
        };

        self.pool.slave_mut(ready_slave_idx).tx.send(Command::Workload(line)).unwrap();
        self.busy.push(ready_slave_idx);
    }

    fn poll(&mut self) -> TaskResult {
        let (busy_idx, result) = self.recv_any();
        let slave_idx = self.busy[busy_idx];
        match result {
            TaskResult::NotFound(..) | TaskResult::Found(..) => 
                self.idle.push(slave_idx),
            TaskResult::Error(..) => {
                self.pool.join_terminate(slave_idx);
                self.free.push(slave_idx);
            },
            TaskResult::Panic => {
                self.pool.join(slave_idx);
                self.free.push(slave_idx);
            },
        }

        self.busy.swap_remove(busy_idx);
        result
    }

    fn recv_any(&mut self) -> (usize, TaskResult) {
        let sel = mpsc::Select::new();
        let mut handles: Vec<_> = self.busy.iter().map(|&idx| self.pool.slave(idx)).map(|slave| sel.handle(&slave.rx)).collect();
        loop {
            for h in handles.iter_mut() { unsafe { h.add(); } }
            let ret = sel.wait();
            for h in handles.iter_mut() { unsafe { h.remove(); } }
            for (busy_idx, handle) in handles.iter_mut().enumerate() {
                if handle.id() == ret {
                    return (busy_idx, handle.recv().unwrap_or(TaskResult::Panic));
                }
            }
        }
    }
}

fn process_result(result: TaskResult) {
    match result {
        TaskResult::NotFound(task) => 
            println!("Task {}: NOT FOUND", task),
        TaskResult::Found(task, password) => 
            println!("Task {}: SUCCESS, password: [ {} ]", task, password),
        TaskResult::Error(input, error) => { 
            let _ = writeln!(&mut io::stderr(), "Input data: [ {} ], error: {:?}", input.trim_right_matches(|c: char| c.is_whitespace()), error); 
        },
        TaskResult::Panic => {
            let _ = writeln!(&mut io::stderr(), "Slave crashed on panic");
        },
    }
}

fn read_line_loop(manager: &mut Manager) -> Result<(), io::Error> {
    let mut reader = io::BufReader::new(io::stdin());
    loop {
        while manager.available() {
            let mut line = String::new();
            match try!(reader.read_line(&mut line)) {
                0 => return Ok(()),
                _ => manager.feed_workload(line),
            }
        }

        process_result(manager.poll());
    }
}

fn run() -> Result<(), io::Error> {
    let mut manager = Manager::new();
    try!(read_line_loop(&mut manager));
    while !manager.all_finished() {
        process_result(manager.poll());
    }
    Ok(())
}

fn main() {
    run().unwrap_or_else(|err| { let _ = writeln!(&mut io::stderr(), "I/O error: {:?}", err); });
}

